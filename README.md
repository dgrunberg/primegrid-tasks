# Processor for task times from primegrid.com

Has with built-in grouping for multiple GPU systems.

Retrieves stderr output from all tasks for this host and correlate by gpu_device_name

Works on certain Linux and Windows tasks.  Certain Linux tasks do not seem to output any stderr data,
so it will not work on those.

We cache stderr outputs in files named resultid\<resultid\>-\<status\>.txt, so that we do not retrieve
files multiple times from the primegrid website.

## Installation
To retrieve:
```
git clone https://dgrunberg@gitlab.com/dgrunberg/primegrid-tasks.git
```
This should put a copy in primegrid-tasks directory.
You can also download a zip file from gitlab project page directly without using git.
Unzip these files in a convenient directory.  Cached files from the web will be placed in this
directory also.

## Requirements
Wants Python 3.5 at a minimum.  It might run with earlier versions but has not been tested.  You
will need to remove the assert version check if you want to test.

For modules, you can do
```
pip install -r requirements.txt
```
to install modules needed.  Or check the requirements file to see what is needed.  
## Use
Use an environment variable PG_HOSTID to specify your hostid or the --host command line option
Use -h option for help.

## Sample output
```
$ ./pg-tasks.py
Processing tasks for hostid 820440
====================================

STATUS                                        count
Completed and validated                          41
Completed, waiting for validation                53
In progress                                       6
TOTAL                                           100

                              run-avg  run-std  run-count  credit-avg  credit-cnt  credit_per_s
date       gpu  task                                                                           
2017-05-10 0    pps_sr2sieve    405.4     1.77         28      3371.0          10         8.315
           1    pps_sr2sieve    417.9     5.79         26      3371.0           8         8.066
           2    pps_sr2sieve    386.8     2.02         29      3371.0          14         8.715
           none llrSGS          659.4    13.04         11        39.9           9         0.061

                   run-avg  run-std  run-count  credit-avg  credit-cnt  credit_per_s
gpu  task                                                                           
0    pps_sr2sieve    405.4     1.77         28      3371.0          10         8.315
1    pps_sr2sieve    417.9     5.79         26      3371.0           8         8.066
2    pps_sr2sieve    386.8     2.02         29      3371.0          14         8.715
none llrSGS          659.4    13.04         11        39.9           9         0.061

Tasks: Processed:100, Completed:94 New files requested:1 Total time:  2.93 sec
```
The columns are:
* date:         the day of the task
* gpu:          the gpu ID, as best can be determined from the stderr output
* task:         initial part of the task name
* run-avg:      average runtime for that group
* run-std:      standard deviation of the runtimes
* run-count:    number of runs
* credit-avg:   average credit for that group (no counting credits == 0)
* credit-cnt:   total count of tasks for which credit > 0
* credit_per_s: average credit divided by average runtime

## To Do
* Give stats on other categories of tasks, such as "Timed out", "In Progress", etc.