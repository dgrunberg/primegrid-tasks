#!/usr/bin/env python
#Copyright 2017 Daniel Grunberg
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
'''
program to parse primegrid.com output to associate GPUs with tasks and times
see
pg-tasks.py --help
'''
import os
import glob
import sys
import re
import time
import datetime
import requests
import argparse

import datetime
import numpy as np
import pandas as pd
import collections

assert sys.version_info >= (3,5)
########## CONSTANTS
D_ERROR=0
D_WARNING=1
D_INFO=2
VERSION='0.2'
########## GLOBALS
verbose_level=D_WARNING
logfile=None
logged=0   # count number of things going into log file

parser = argparse.ArgumentParser(
    description="pg-tasks",
    formatter_class=argparse.RawTextHelpFormatter,
    epilog='''
Processor of task times from primegrid.com.  
Retrieves stderr output from all tasks for this host and correlate by 
gpu_device_name and task name.  Caches all retrieved stderr outputs in files 
named resultid<resultid>-<status>.txt, so that we do not re-retrieve it multiple 
times from the website.  Example file name: resultid8123123-w.txt

You may use an environment variable PG_HOSTID to specify your hostid or 
the --host command line option

Grabs tasks by primegrid page, 20 tasks at a time, use more pages to go further back in time
Logs by appending to file by default pg-tasks.log, which can be changed with --log option.
''')
########## FUNCTION DEFINITIONS
def get_value(pattern, page, item, file, quiet=False):
    #return matching regex pattern (using one group) from page, or None if it can't be found
    #if quiet False, prints an error message when data missing, uses item, file in message
    m=re.search(pattern,page)
    if m:
        return m.group(1)
    else:
        if not quiet:
            log(D_WARNING,'could not get {} from {}'.format(item, file))
        return None

#smallest initial string that leaves only digits and underscores until the end
task_pattern=re.compile(r'(.*?)[\d_]*$')
def task_name(s):
    #create a task name from the full result name E.g. pps_sr2sieve_100729799_1 => pps_sr2sieve
    m=task_pattern.match(s)
    if m:
        ret=m.group(1)
    else:
        ret='unk'
    return ret

def convert_date(s):
    #compute datetime from the primegrid string s:, E.g. /7 May 2017 | 14:22:00 UTC/
    #x=s.split()
    try:
        ret=datetime.datetime.strptime(s, '%d %b %Y | %H:%M:%S %Z')
    except ValueError as ex:
        log(D_ERROR, 'Got exception on date conversion /{}/ {}'.format(s, ex))
        ret=datetime.datetime(2000,1,1)
    return ret

def log(level, s):
    #output the debug string s if level equals or exceed global verbose_level
    #the string goes into the global log file - if options.log not set, then nothing done
    global logfile
    global logged
    if options.log is None:
        return
    if level > verbose_level:
        #not severe enough, do nothing
        return
    if logfile is None:
        logfile=open(options.log, 'a')
        logfile.write('==================================================\n')
        logfile.write('Starting task analysis {}\n'.format(datetime.datetime.now()))
    logfile.write('{}\n'.format(s))
    logged+=1
    
def totalize(df, cols):
    #group the dataframe df by cols, a list of columns
    #print out the dataframe
    grouped=df.groupby(cols)
    g=grouped.agg({'runtime':{'run-avg': np.mean, 'run-std': np.std, 'run-count': 'count'},
                   'credit':{'credit-sum':np.sum, 'credit-cnt': lambda x: sum(x > 0)}})
    g.columns=g.columns.droplevel(0)
    g['credit-cnt']=g['credit-cnt'].astype(int)
    g['credit-avg']=g['credit-sum']/g['credit-cnt']
    g['credit_per_s']=g['credit-avg']/g['run-avg']
    g.drop('credit-sum', axis=1, inplace=True)  # 1=column
    #set the correct order of columns we want
    cols=['run-avg', 'run-std', 'run-count', 'credit-avg', 'credit-cnt', 'credit_per_s']
    g=g[cols]
    g=g.round({'run-avg': 1, 'run-std':2, 'credit-avg':1, 'credit_per_s':3})
    print()
    print(g)
    
########## START MAIN CODE
start=datetime.datetime.now()
pd.options.display.max_columns=15
pd.options.display.max_rows=999
pd.options.display.expand_frame_repr=False
parser.add_argument('--host', help='hostid to grab (overrides var PG_HOSTID)', default=None)
parser.add_argument('--pages', type=int, help='number of 20-task pages to analyze', default=1)
parser.add_argument('--log', type=str, help='file to store log messages', metavar='FILE', default='pg-tasks.log')
parser.add_argument('-v', '--verbose', action='store_true', help='output more debug messages if if --log used)')

options = parser.parse_args()
if options.verbose:
    verbose_level=D_INFO
    
if options.host is None:
    if 'PG_HOSTID' not in os.environ:
        sys.stderr.write('must specify hostid on command line or use environment var PG_HOSTID\n')
        sys.stderr.write('use --help\n')
        sys.exit(1)
    options.host=os.environ['PG_HOSTID']

#Let's pull out resultid=() and status () with this regex
patt=re.compile('resultid=(\d+).*?</a></td><td>.*?</td><td>.*?</td><td>.*?</td><td>(.*?)</td')
requested_count=0
processed_count=0
completed_count=0
print('Processing tasks for hostid {} V {}'.format(options.host, VERSION))
print('========================================\n')
#create a list of dicts, each item one row in our Pandas table
pd_data=[]
collect_stats=collections.defaultdict(int)   # dictionary of count of each status for reporting at the end
for i in range(options.pages):
    #This gets all the tasks for hostid
    url='http://www.primegrid.com/results.php?hostid={}&offset={}'.format(options.host, i*20)        
    r=requests.get(url)
    page=str(r.content)
    if r.status_code != 200:
        sys.stderr.write('Error {}\n'.format(page))
        sys.exit(1)
    if i==0 and re.search('No computer with ID', page):
        sys.stderr.write('No computer with ID {} found\n'.format(options.host))
        sys.exit(1)
    for id, status in patt.findall(page):
        #we have pulled out resultid and status fields
        #print('looking for resultid={} status={}'.format(id, status))
        processed_count+=1
        collect_stats[status]+=1   # count it
        if status=='In progress' or status=='Abandoned' or status[0:5]=='Timed':
            #skip it
            continue
        #we stored the results in a file called resultid<id><status>.txt, where status is v if
        #status was Completed and validated, otherwise w (waiting).  That way we will still get an
        #update when the status status switches from w to v.
        code='v' if status=='Completed and validated' else 'w'
        result_file='resultid{}-{}.txt'.format(id, code)
        if not os.path.isfile(result_file):
            url2='http://www.primegrid.com/result.php?resultid={}'.format(id)
            #print(url2)
            r2=requests.get(url2)
            result_page=str(r2.content)
            if r2.status_code!=200:
                log(D_ERROR, 'got error {} {}'.format(r2.status_code, result_page))
            else:
                with open(result_file, 'w') as fp:
                    fp.write(result_page)
                    log(D_INFO, 'retrieved {} and stored in {}'.format(url2, result_file))
                requested_count+=1
            time.sleep(0.1)  # rate control accessing files from server to be nice
        #now read the file and process
        with open(result_file, 'r') as res:
            completed_count+=1
            results=res.read()
            #NOTE: field has LFs as literal \n's
            #NOTE: Windows output has different string - Detected GPU 3:
            gpu=get_value(r'gpu_device_num.{0,3}Skipping:\W+(\d+)',results, 'gpu_device_num', result_file, quiet=True)
            if gpu is None:
                #Try the Windows string version
                gpu=get_value(r'Detected GPU\W+(\d+)',results, 'gpu_device_num', result_file, quiet=True)
            #result_name=get_value(r'result_name.{0,3}Skipping:\W+(\w+)\W',results, 'result_name', result_file)
            result_name=get_value(r'Name.*?</td>.*?>(.*?)<',results, 'Name', result_file)
            run_time=get_value(r'Run time.*?>([\d.,]+)',results, 'Run Time', result_file)
            run_time=float(run_time.replace(',',''))
            date=get_value(r'Received.*?</td>.*?>(.*?)<',results, 'Receive Date', result_file)
            credit=get_value(r'Credit.*?</td>.*?>(.*?)<',results, 'Credit', result_file)
            # remove commas and make a float
            credit=float(credit.replace(',',''))
            if any([result_name is None, run_time is None, date is None]):
                continue
            if gpu is None:
                gpu='none'
            #create a task name from the full result name.
            task=task_name(result_name)
            dt=convert_date(date).date()   # just want to do by day (no time)
            log(D_INFO, '{} {:8} {:4} {:30} {!s:13} {:8} {:8}'.format(date,id,gpu,result_name,task,run_time,credit))
            pd_data.append({'date':dt,'task': task,'gpu':gpu,'runtime':run_time,'credit':credit})
    time.sleep(0.3)
tot=0
print('STATUS                                        count')
for k,v in collect_stats.items():
    print('{:40} {:10}'.format(k, v))
    tot+=v
print('TOTAL                                    {:10}'.format(tot))
if len(pd_data)==0:
    sys.stderr.write('\nGot no run time data.  Might want to try increasing --pages option.\n')
    sys.exit(1)
df=pd.DataFrame(pd_data, columns=['date', 'gpu', 'task', 'runtime', 'credit'])
log(D_INFO, repr(df))
                      
    
totalize(df, ['date', 'gpu', 'task'])
totalize(df, ['gpu', 'task'])
dur=(datetime.datetime.now()-start).total_seconds()
print('\nTasks: Processed:{}, Completed:{} New files requested:{} Total time:{:6.2f} sec'.format(processed_count,
                                                                                         completed_count,requested_count,dur))
if completed_count==0:
    print('Note: you can increase --pages argument to get some completed tasks')
if logged > 0:
    print('There were {} messages added to the log file {}'.format(logged, options.log))
            
            
                
        
    




